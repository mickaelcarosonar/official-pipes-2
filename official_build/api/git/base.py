'''
Git
'''
import subprocess


class GitApiServiceError(Exception):
    pass


class GitApiService:
    def get_pipe_published_at(self, infile, date_format='iso-strict') -> str:
        '''
        Get pipe's official published date
        param: infile: File path of the pipe's metadata file
        param: date_format: Date format git's supported
        return: Date of the commit of merged pipe's file to master branch
        '''
        # git log --reverse --merges --first-parent master --format=%aD pipes/aws-cloudformation-deploy.yml | head -1
        logs_cmd = f"git log --reverse --merges --first-parent master --date={date_format} --format=%ad -- {infile}"
        head_cmd = "head -1"
        return self._execute_git_command(logs_cmd, head_cmd)

    def get_pipe_file_added_at(self, infile, date_format='iso-strict') -> str:
        '''
        Get pipe's file added date
        param: infile: File path of the pipe's metadata file
        param: date_format: Date format git's supported
        return: Date of the commit of added pipe's file to git
        '''
        # git log --revers --date='iso-strict' --format=%aD pipes/aws-cloudformation-deploy.yml | head -1
        logs_cmd = f"git log --reverse --date={date_format} --format=%ad -- {infile}"
        head_cmd = "head -1"
        return self._execute_git_command(logs_cmd, head_cmd)

    def get_pipe_updated_at(self, infile: str) -> str:
        raise NotImplementedError()

    @staticmethod
    def _execute_git_command(logs_cmd, head_cmd):
        # git log | head -1
        logs = subprocess.Popen(logs_cmd.split(' '), stdout=subprocess.PIPE,)
        result = subprocess.run(head_cmd.split(' '), stdin=logs.stdout, capture_output=True, text=True, encoding='utf-8')

        if result.returncode != 0:
            raise GitApiServiceError(result.stderr)

        return result.stdout.strip()
